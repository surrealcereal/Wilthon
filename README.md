# Wilthon
A Python-based backup utility meant to bypass the permadeath feature of Ghost Mode of Ghost Recon: Wildlands.

The script is re- and further customizable through the `options.ini` file in `%appdata%\Wilthon`.

Logs pertaining to Wilthon can be found under `%appdata%\Wilthon\Logs`.

## Basic Usage
First of all, [disable cloud saves](https://www.ubisoft.com/en-us/help/connectivity-and-performance/article/disabling-cloud-synchronisation-in-ubisoft-connect-pc/000063189#:~:text=Open%20the%20side%20menu%2C%20marked%20with%20an%20icon%20of%20three%20lines.&text=Select%20Settings.&text=In%20the%20General%20section%2C%20untick,the%20synchronisation%20will%20be%20disabled), as Wildlands restoring cloud saves would interfere with a local restoration performed by Wilthon. 

Wilthon automatically restores backups when Wildlands is shut down. So, when you die, you close the game, and Wilthon restores a recent savegame and relaunches Wildlands. As easy as that!

When you want to simply stop playing, select `(2)` from the backup options after closing Wildlands and Wilthon shuts down.

Additionally, pressing the key below Escape (` for the English keyboard layout) can be used to trigger a short beeping sound and check if the script is running in the background without having to Alt-Tab.

## `options.ini`

### Game Installation Related Options `[Install]`:

**Please note that these values are saved here for the script to work properly. 
Thus, they should only be changed if one knows what they are doing.**

#### `provider`

Specifies the vendor of the Wildlands installation. Can be `steam` or `ubisoft`.

#### `savegame_location`

Points to the location of the savegame files for Wildlands. Can be `3559` or `1771`.

`C:\Program Files (x86)\Ubisoft\Ubisoft Game Launcher\savegames\{USER_ID}` stores all savegames for all Ubisoft produced games installed onto the system.
The subdirectory `3559` contains savegames of Wildlands installations from Steam, whereas `1771` serves the same purpose for Uplay/Ubisoft Connect installations.

#### `grw_location`

Points to the path of the executable for the game, `GRW.exe`.

#### `steam_location`
Points to the path of the Steam executable `Steam.exe` to be used for launching Wildlands for Steam installations.

---

### Backup Making Related Options `[Backup]`:
#### `backup_interval` 
_Non-Advanced Install Default: `30`_

The time in seconds to wait between two consecutive backups.

#### `max_backup_count` 
_Non-Advanced Install Default: `20`_

The number of maximum backups allowed. If this number is exceeded, the oldest backup will be deleted until that is no longer the case.

---

### Backup Restoring Related Options `[Restore]`:
#### `default_restore_selection`
_Default: `1`_

Specifies the option to choose after the timeout specified in `restore_timeout` expires, as indicated in the restore prompt in parentheses.

`1` is "Automatically Restore and Relaunch Game", `2` is "Exit Wilthon", and `3` is "Manually Choose Backup to Restore From and Relaunch Game".

#### `restore_timeout`
_Default: `7`_

The time in seconds after which the option specified in `default_restore_selection` is automatically selected if no user input is provided.

#### `folder_index_to_restore`
_Default: `2`_

The 1-indexed value specifying which backup to restore from automatically. `1` means restoring the latest backup, `2` means the second latest _etc._

**Setting this to `1` is not recommended** as a backup may occur in the sub-second time Wilthon takes to realize the game has been shut down after permadeath has occurred. 
  If this is the case, the restored savegame being the latest would mean Wilthon would not have avoided permadeath, nullifying its purpose.

---

### Update Related Options `[Update]`:
#### `do_update`
_Default: `True`_

Specifies whether to check for updates or not. Can be `True` or `False`.

#### `autoupdate` 
_Default: `True`_

Specifies whether updates are carried out automatically or a prompt asking the user is shown before updating. Can be `True` or `False`.

---

### Miscellaneous Options `[Misc]`:
#### `assure_running_keybind`
_Default: The key below Escape, `` ` `` for the English keyboard layout_

Specifies the key to press to get the age of the latest backup and play a beeping sound to confirm that Wilthon is running, provided that `play_sound` is set to `True`. 

Note that changes to the keyboard layout during runtime are not registered, meaning the key will not move locationally.
Thus, keep in mind that the position of the key will depend on the keyboard layout used at launch. 

Also note that the key that is below Escape is deduced based upon your keyboard layout at the time of install.
Hence, if this setting is left at its default, ensure that you launch Wilthon using the same keyboard layout you used to install it.

#### `play_sound`
_Default: `True`_

Specifies whether to play a beeping sound when the key specified in `assure_running_keybind` is pressed. Can be `True` or `False`.
#### `launch_on_launch`
_Default: `True`_

Specifies whether to launch Wildlands if it is not running when Wilthon is launched. Can be `True` or `False`.