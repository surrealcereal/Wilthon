import communication
import install
import option
import routine
import update
import utility


def pre_setup():
    utility.command_line_setup()
    communication.logger_init()
    if not utility.is_installed():
        install.create_dir_structure()
        return False
    return True


@utility.guarded(uses_state=False)
def main():
    installed = pre_setup()
    if not installed:
        options = install.install()
    else:
        options = option.Options()
    update.perform(options)
    routine.initialize(options)


if __name__ == "__main__":
    main()
