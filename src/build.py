import os
import pathlib

import PyInstaller.__main__

from version import __version__


def create_exe(source_file, name):
    PyInstaller.__main__.run([
        source_file,
        "--onefile",
        "--distpath",
        ".",
        r"--add-data=resources\beep.wav;resources",
        r"--icon=resources\logo.ico",
        "--name",
        name
    ])


def main():
    cwd = pathlib.Path(os.getcwd())
    src_dir = cwd / "src"
    name = f"wilthon-v{'.'.join(map(str, __version__.values()))}"
    create_exe(str(src_dir / "__init__.py"), name)


if __name__ == "__main__":
    main()
