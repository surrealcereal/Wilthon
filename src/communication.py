import pathlib
import time

import keyboard

import strings
import utility
import windows


# Not using `logging` to not have to deal with its complexities. A simple reimplementation as such works just fine
# for our purposes.
class Logger:
    def __init__(self, path):
        self.path = path

    def _log(self, msg, level=None):
        with open(self.path, "a") as f:
            if level:
                f.write(add_info_to_str(msg, level) + "\n")
            else:
                f.write(msg + "\n")

    def info(self, msg):
        self._log(msg, "info")

    def error(self, msg):
        self._log(msg, "error")

    def puts(self, msg):
        self._log(msg)


def logger_init():
    time_str = time.strftime("%Y-%m-%d %H.%M.%S")
    log = pathlib.Path(strings.consts["root"]) / "Logs" / (time_str + ".log")
    return Logger(log)


logger = logger_init()


def colorer(message, color):
    color_codes = {
        "cyan": '\033[96m',
        "green": '\033[92m',
        "yellow": '\033[93m',
        "red": '\033[91m',
        "end": '\033[0m'
    }
    return color_codes[color] + message + color_codes["end"]


def add_info_to_str(message, level, color=None):
    message = f"{time.strftime('%Y-%m-%d %H.%M.%S')} [{level.upper()}]\t {message}"
    if not color:
        return message
    return colorer(message, color)


def formatter(message, fmt):
    if isinstance(fmt, str):
        return message.format(fmt)
    else:
        return message.format(*fmt)


def info(code, fmt=str(), color=""):
    raw = strings.infos[code]
    message = add_info_to_str(raw, "info", color)
    if fmt:
        message = formatter(message, fmt)
    print(message)
    logger.info(raw)


def error(code, fmt=str()):
    color = "red"
    raw = strings.errors[code]
    message = add_info_to_str(raw, "error", color)
    if fmt:
        message = formatter(message, fmt)
    print(message)
    logger.error(raw)


def prompt(code, fmt=str()):
    color = "green"
    raw = strings.prompts[code]
    prompt_text = add_info_to_str(raw, "input", color) + " "
    if fmt:
        prompt_text = formatter(prompt_text, fmt)
    return input(prompt_text)


def puts(code, fmt=str(), end="\n", color=None):
    message = strings.strings[code]
    if fmt:
        message = formatter(message, fmt)
    if color:
        message = colorer(message, color)
    print(message, end=end)


def mimic(code, type, fmt=str(), end="\n", color=None):
    raw = strings.strings[code]
    prompt_text = add_info_to_str(raw, type, color) + " "
    if fmt:
        prompt_text = formatter(prompt_text, fmt)
    if color:
        prompt_text = colorer(prompt_text, color)
    print(prompt_text, end=end)


# The magic numbers in the following function are there to align the traceback lines accurately to how they are
# normally aligned.
def get_current_padding(level):
    test_case = add_info_to_str(strings.infos["quit"], level)
    info_header = test_case.rstrip(strings.infos["quit"])
    offset = 3 if windows.is_running_from_command_line() else -1
    return " " * (len(info_header.expandtabs(6)) + offset)


def exception(ex):
    color = "red"
    traceback = utility.get_traceback(ex)
    first_line = traceback[0]
    logger.error(first_line)
    print(add_info_to_str(first_line, "error", color))
    padding = get_current_padding("error")
    log_padding = padding + " " * 4
    for line in traceback[1:]:
        print(colorer(padding + line, color))
        logger.puts(log_padding + line)


def handle_exception():
    error("crash")
    mimic("press_key", "input", color="green")
    if windows.is_wilthon_focused():
        keyboard.read_key(suppress=True)

def multiline_preprompt(prompt_code):
    color = "green"
    lines = strings.strings[prompt_code].splitlines()
    first_line = lines[0]
    print(add_info_to_str(first_line, "input", color))
    padding = get_current_padding("input")
    for line in lines[1:]:
        print(colorer(padding + line, color))


def y_n_prompt(code, fmt=str()):
    color = "green"
    prompt_text = add_info_to_str(strings.prompts[code], "input")
    if fmt:
        prompt_text = formatter(prompt_text, fmt)
    prompt_text += " (y/n) "
    prompt_text = colorer(prompt_text, color)
    while (ret := input(prompt_text)).lower() not in ("y", "yes", "n", "no"):
        error("invalid_input")
    return ret.lower() in ("y", "yes")


def time_index_question(backup_interval, is_min, question_code, confirmation_code):
    # `interval_display` must be a float for now, it will be checked whether it is an int later.
    interval_display = backup_interval / 60 if is_min else float(backup_interval)
    while True:
        index_answer = prompt(question_code)
        if not index_answer.isdigit():
            error("invalid_input")
            continue
        index_answer = int(index_answer)
        final_time = index_answer * interval_display
        if final_time.is_integer():
            final_time = int(final_time)
        kw = {"minute" if is_min else "second": final_time}
        time_to_display = utility.time_formatter(**kw)
        confirmation = y_n_prompt(confirmation_code, fmt=time_to_display)
        if confirmation:
            return index_answer
