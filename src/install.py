import pathlib

import communication
import option
import strings
import utility
import windows


def create_dir_structure():
    root = pathlib.Path(strings.consts["root"])
    root.mkdir()
    (root / "Logs").mkdir()
    communication.info("created_dirs")


def get_exe_location(exe_name, exe_found_code, not_running_code, not_in_path_code):
    while True:
        exe_path = windows.find_exe_path(exe_name)
        if exe_path:
            communication.info(exe_found_code)
            return str(exe_path)
        communication.error(not_running_code)
        answer = communication.prompt(not_running_code)
        if answer.lower() == "r":
            continue
        answer_path = pathlib.Path(answer)
        if not answer_path.exists():
            communication.error("path_doesnt_exist")
            continue
        if windows.dir_contains_file(answer_path, exe_name):
            return answer
        communication.error(not_in_path_code)


def get_grw_location(provider):
    def get_guess_location(guess_path_code):
        for drive in windows.get_drives():
            path = drive / strings.consts[guess_path_code]
            if windows.dir_contains_file(path, "GRW.exe"):
                return True
        return False

    code = "steam_grw_path" if provider == "steam" else "ubisoft_grw_path"
    grw_loc = get_guess_location(code)
    if grw_loc:
        communication.info("grw_found")
        return strings.consts[code]
    return get_exe_location(exe_name="GRW.exe", exe_found_code="grw_found", not_running_code="grw_not_running",
                            not_in_path_code="no_grw_in_path")


def get_savegame_upper_dir():
    drives = windows.get_drives()
    for drive in drives:
        # drive \ Program Files (...)       when installed with Steam onto main drive or with Ubisoft onto any drive
        # drive \ Ubisoft Game Launcher     when installed with Steam onto drive other than the main one
        program_file_path = drive / "Program Files (x86)" / "Ubisoft" / strings.consts["savegames_super_dir"]
        if program_file_path.exists():
            return program_file_path
        upper_dir = drive / strings.consts["savegames_super_dir"]
        if upper_dir.exists():
            return upper_dir
    
    communication.error("no_savegame_upperdir", fmt=", ".join(map(str, drives)))
    communication.handle_exception()


def get_savegame_info():
    def is_ubisoft(path):
        return path.name.endswith("1771") and path.is_dir()

    def is_steam(path):
        return path.name.endswith("3559") and path.is_dir()

    upper_dir = get_savegame_upper_dir()
    uuid_subdirs = windows.get_subdirs(upper_dir)
    subdirs = [x for y in uuid_subdirs for x in windows.get_subdirs(y)]
    candidates = list()
    for d in subdirs:
        if is_steam(d):
            communication.info("steam_savegame_detected")
            candidates.append(("steam", d))
        if is_ubisoft(d):
            communication.info("ubisoft_savegame_detected")
            candidates.append(("ubisoft", d))

    if len(candidates) == 1:
        return candidates[0]
    if len(candidates) == 0:
        communication.error("no_savegame_dir")
        communication.info("no_savegame_dir_info")
    else:
        communication.error("multiple_savegame_dirs")

    while True:
        answer = communication.prompt("custom_savegame_dir")
        answer_path = pathlib.Path(answer)
        if not answer_path.exists():
            communication.error("path_doesnt_exist")
            continue
        if not is_steam(answer_path) or is_ubisoft(answer_path):
            communication.error("not_valid_savegame_dir")
            continue
        provider = "ubisoft" if is_ubisoft(answer_path) else "steam"
        return provider, answer


def get_steam_location():
    for drive in windows.get_drives():
        steam_path = drive / strings.consts["steam_path"]
        steam_status = windows.dir_contains_file(steam_path, "steam.exe")
        if steam_status:
            return strings.consts["steam_path"]
    return get_exe_location(exe_name="steam.exe", exe_found_code="steam_found", not_running_code="steam_not_running",
                            not_in_path_code="no_steam_in_path")


def is_advanced_setup():
    return communication.y_n_prompt("advanced_setup_question")


def backup_interval_question():
    min_mode = True
    while True:
        answer = communication.prompt("interval_question_min" if min_mode else "interval_question_sec")
        if utility.isfloat(answer):
            modifier = 60 if min_mode else 1
            return min_mode, float(answer) * modifier
        if min_mode and answer.lower() == "s":
            min_mode = False
            continue
        if not min_mode and answer.lower() == "m":
            min_mode = True
            continue
        communication.error("invalid_input")


def folder_to_restore_question(backup_interval, is_min):
    return communication.time_index_question(backup_interval, is_min=is_min, question_code="restoration_index_question",
                                             confirmation_code="restoration_index_confirmation")


def max_backup_count_question(backup_interval, is_min):
    return communication.time_index_question(backup_interval, is_min=is_min, question_code="max_backup_count_question",
                                             confirmation_code="max_backup_count_confirmation")


def launch_on_launch_question():
    return communication.y_n_prompt("launch_on_launch_question")


def install():
    provider, savegame_location = get_savegame_info()
    grw_location = get_grw_location(provider)
    steam_location = get_steam_location() if provider == "steam" else None
    if not is_advanced_setup():
        backup_interval = 30
        max_backup_count = 20
    else:
        is_min, backup_interval = backup_interval_question()
        max_backup_count = max_backup_count_question(backup_interval, is_min)

    options = option.Options(provider, savegame_location, grw_location, steam_location, backup_interval,
                             max_backup_count)
    communication.info("successfully_installed")
    return options

# TODO: BB: what will happen if the game is cracked? Provider will be set to the origin of the crack but if the
#  origin is steam trying to launch the game through steam.exe -applaunch won't work
