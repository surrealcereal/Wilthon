import configparser

import strings
import utility
import windows


class Options:
    _layout = {
        "Install": ("provider", "savegame_location", "grw_location", "steam_location"),
        "Backup": ("backup_interval", "max_backup_count"),
        "Restore": ("default_restore_selection", "restore_timeout", "folder_index_to_restore"),
        "Update": ("do_updates", "autoupdate"),
        "Misc": ("assure_running_keybind", "play_sound", "launch_on_launch")
    }

    def __init__(self, provider=None, savegame_location=None, grw_location=None, steam_location=None,
                 backup_interval=None, max_backup_count=None):

        if provider is None:
            self.read_from_ini()
            return

        # [Install]
        self.provider = provider
        self.savegame_location = savegame_location
        self.grw_location = grw_location
        self.steam_location = steam_location

        # [Backup]
        self.backup_interval = backup_interval
        self.max_backup_count = max_backup_count

        # [Restore]
        self.default_restore_selection = 1
        self.restore_timeout = 7
        self.folder_index_to_restore = 2

        # [Update]
        self.do_updates = True
        self.autoupdate = True

        # [Misc]
        self.assure_running_keybind = windows.scan_code_to_key(41)  # Ensure key below Esc.
        self.play_sound = True
        self.launch_on_launch = True

        self.write_to_ini()

    def write_to_ini(self):
        config = configparser.ConfigParser()

        for section, values in Options._layout.items():
            temp = dict()
            for attribute_name in values:
                val = getattr(self, attribute_name)
                # `steam_location` is None for Ubisoft installs, which is not accepted as a valid value by configparser.
                # So, convert it to an empty string before serialization.
                temp[attribute_name] = "" if val is None else val
            config[section] = temp

        with open(strings.consts["config_location"], "w") as f:
            config.write(f)

    @staticmethod
    def convert_str_types(str_dict):
        ret_dict = dict()
        for k, v in str_dict.items():
            if v in ("True", "False"):
                ret_dict[k] = eval(v)
                continue
            if v.isnumeric():
                ret_dict[k] = int(v)
                continue
            if utility.isfloat(v):
                ret_dict[k] = float(v)
                continue
            # Only option left is that `v` is a string.
            ret_dict[k] = v
        return ret_dict

    def read_from_ini(self):
        config = configparser.ConfigParser()
        config.read(strings.consts["config_location"])
        # The .exe version sometimes fails to load the file, so we force-load it here.
        if utility.is_pyinstaller():
            while True:
                try:
                    config.items("Install")
                except configparser.NoSectionError:
                    config.read(strings.consts["config_location"])
                else:
                    break
        for section in Options._layout.keys():
            section_dict = dict(config.items(section))
            init_vals = self.convert_str_types(section_dict)
            for pair in init_vals.items():
                setattr(self, *pair)
