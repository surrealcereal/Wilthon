import multiprocessing
import os
import pathlib
import shutil
import threading
import time

import communication
import running_state
import strings
import tasks
import utility
import windows


# TODO: BB: look into making a decorator accepting funcs that can block and do the while True


@utility.guarded()
def back_savegames_up(options, state):
    def wait_for_backup(options, paused):
        time.sleep(options.backup_interval)
        paused.clear()

    paused = threading.Event()

    while True:
        if state.is_exit():
            state.am_ready("back_savegames_up")
            return
        if state.is_immunity_active() or state.is_suspended():
            utility.wait()
            continue
        if paused.is_set():
            utility.wait()
            continue
        backup_name = f"{time.strftime('%Y-%m-%d %H.%M.%S')}"
        root = pathlib.Path(strings.consts["root"])
        backup_path = pathlib.Path(root / backup_name)
        try:
            shutil.copytree(options.savegame_location, backup_path)
        except (FileExistsError, shutil.Error):
            pass
        communication.info("successful_backup")
        threading.Thread(target=wait_for_backup, args=(options, paused)).start()
        paused.set()


@utility.guarded()
def delete_old_backups(options, state):
    while True:
        if state.is_exit():
            state.am_ready("delete_old_backups")
            return
        if state.is_suspended():
            utility.wait()
            continue

        backups = windows.get_backup_list()
        if not len(backups) > options.max_backup_count:
            utility.wait()
            continue

        oldest_folder = min(backups, key=os.path.getctime)
        time_to_display = windows.get_dir_age_str(oldest_folder)
        try:
            shutil.rmtree(oldest_folder)
        except (FileNotFoundError, PermissionError):
            pass
        communication.info("removed_oldest", fmt=time_to_display)


@utility.guarded()
def handle_game_exits(options, state):
    while True:
        if state.is_exit():
            state.am_ready("handle_game_exits")
            return
        if state.is_immunity_active():
            utility.wait()
            continue

        if windows.is_wildlands_running():
            utility.wait()
            continue

        state.suspend()
        func, arg, unpause = tasks.get_restore_option(options, state)
        func(arg)
        if unpause:
            state.introduce_immunity()
            tasks.launch_game(options)
            state.unsuspend()


@utility.guarded()
def assure_running(options, state):
    # This function may be terminated at will, cutting the playing of the sound in half. This is why the `state`
    # parameter goes unused.
    def perform(options, last_playing_process):
        if not windows.is_wildlands_focused():
            return
        if options.play_sound:
            # This block of code will either:
            # successfully call `terminate` on an existing process, which will stop the sound for it to start anew.
            # unsuccessfully call `terminate` on a process that is not running anymore, which does not raise any
            # exceptions.
            # or, be `None`, as initialized, which is skipped.

            if last_playing_process is not None:
                last_playing_process.terminate()

            last_playing_process = tasks.get_started_player_process()

        backups = windows.get_backup_list()
        newest_folder = max(backups, key=os.path.getctime)
        time_to_display = windows.get_dir_age_str(newest_folder)
        communication.info("running", fmt=time_to_display, color="cyan")
        return last_playing_process

    last_playing_process = None
    while True:
        # Blocking call to `do_once`, waiting not necessary.
        last_playing_process = tasks.do_once(options.assure_running_keybind, perform,
                                             args=(options, last_playing_process))


@utility.guarded()
def initialize(options):
    if utility.is_pyinstaller():
        multiprocessing.freeze_support()
    loops = [handle_game_exits, delete_old_backups, back_savegames_up, assure_running]
    # Account for the fact that `assure_running` can be terminated at will and does not need to be waited upon with
    # `-1`.
    named_awaited_loops = [f.__name__ for f in loops[:-1]]
    state = running_state.State(named_awaited_loops)
    threads = [threading.Thread(target=i, args=(options, state)) for i in loops]

    tasks.launch_on_launch_check(options, state)

    for t in threads:
        t.daemon = True
        t.start()

    while True:
        if not state.is_exit():
            utility.wait()
            continue
        if state.is_ready():
            break
