import multiprocessing
import threading

import communication
import utility
import windows


class State:
    def __init__(self, processes):
        self.process_count = len(processes)
        self._suspended = threading.Event()
        self._exit = threading.Event()
        self._immunity = threading.Event()
        # Using a manager here seems to prevent the value from not incrementing correctly.
        self._readied = multiprocessing.Manager().dict({k: False for k in processes})

    def is_suspended(self):
        return self._suspended.is_set()

    def is_immunity_active(self):
        return self._immunity.is_set()

    def am_ready(self, name):
        self._readied[name] = True

    def is_terminatable(self, name):
        return name not in self._readied

    def is_ready(self):
        for v in self._readied.values():
            if not v:
                return False
        return True

    def introduce_immunity(self):
        def target():
            self._immunity.set()
            consecutive = 0
            # 3s response time should be good enough.
            while consecutive < 30:
                if windows.is_wildlands_running():
                    consecutive += 1
                else:
                    consecutive = 0
                utility.wait()
            self._immunity.clear()

        threading.Thread(target=target).start()
        communication.info("immunity")

    def is_exit(self):
        return self._exit.is_set()

    def suspend(self):
        self._suspended.set()

    def unsuspend(self):
        self._suspended.clear()

    def exit(self):
        self._exit.set()
