import os

errors = {
    "path_doesnt_exist": "The entered path does not exist.",
    "grw_not_running": "No running iteration of Ghost Recon: Wildlands detected. An iteration needs to be running or the path to the game executable must be given for the setup to continue.",
    "no_grw_in_path": "Specified path does not contain Ghost Recon: Wildlands executable (GRW.exe).",
    "no_savegame_upperdir": "No available savegame superdir in following detected drives: {}.",
    "no_savegame_dir": r"No save game files for Ghost Recon: Wildlands found in C:\Program Files (x86).",
    "multiple_savegame_dirs": r"Multiple save game files for different installations of Ghost Recon: Wildlands found in C:\Program Files (x86).",
    "not_valid_savegame_dir": "Specified path is not a valid save game directory.",
    "no_steam_running": "No running iteration of Steam detected. An iteration needs to be running or the path to the Steam executable must be given for the setup to continue.",
    "invalid_input": "Please enter a valid input.",
    "crash": "Wilthon has exited abruptly for the reason stated above, which has also been saved to a log file."
}

infos = {
    "unsuccessful_install": "It appears that the last attempt to install Wilthon has proven unsucessful.",
    "grw_found": "Path containing Ghost Recon: Wildlands executable found.",
    "steam_found": "Path to Steam executable found.",
    "ubisoft_savegame_detected": "A Ubisoft install of Ghost Recon: Wildlands detected.",
    "steam_savegame_detected": "A Steam install of Ghost Recon: Wildlands detected.",
    "no_savegame_dir_info": "This may be because the game has never been launched before.",
    "created_dirs": "Created Wilthon directories.",
    "successfully_installed": "Successfully installed Wilthon, initiating backup routine...",
    "successful_backup": "Successfully backed save games up.",
    "removed_oldest": "Successfully removed oldest backup, backed up {} ago.",
    "running": "Wilthon is still running, last backup was {} ago.",
    "no_grw_running": "No running iteration of Ghost Recon: Wildlands detected. Initiating backup options...",
    "immunity": "Pausing functionality briefly to allow Wildlands to launch.",
    "launching_game": "Launching Wildlands...",
    "waiting_for_grw": "Waiting for Wildlands to be launched...",
    "restored": "Successfully restored save game.",
    "updating": "Updating Wilthon from v{} to v{}...",
    "successful_update": "Successfully updated Wilthon from v{} to v{}.",
    "quit": "Exiting Wilthon."
}

prompts = {
    "restarting_install": "Do you want to retry the install? Accepting will delete all Wilthon related files. Declining will exit Wilthon.",
    "grw_not_running": "R to retry, or input path to game executable.",
    "custom_savegame_dir": "Enter the path to your save games folder.",
    "advanced_setup_question": "Do you want to enter advanced setup? Sane defaults will be chosen otherwise.",
    "interval_question_sec": "How often should backups be made? (in seconds, enter \"m\" to switch to minutes.)",
    "interval_question_min": "How often should backups be made? (in minutes, enter \"s\" to switch to seconds.)",
    "restoration_index_question": "How old should the backup be when restoring? Answer with a number indicating the index of the backup to choose, e.g. 1 for the newest.",
    "restoration_index_confirmation": "This equates to the script automatically rolling back to a save game from {} ago, are you sure?",
    "max_backup_count_question": "How many backups should be kept at once?",
    "max_backup_count_confirmation": "This means backups will be kept for {} and then deleted, are you sure?",
    "launch_on_launch_question": "Would you like Wilthon to automatically launch Ghost Recon: Wildlands if it isn't running when started?",
    "manual_restore_question": "Which backup should be restored? Answer with a number indicating the index of the backup to choose, e.g. 1 for the newest.",
    "manual_restore_confirmation": "This equates to a roll back of {}, are you sure?",
    "update_question": "A new version of Wilthon is available: v{}. Would you like to do the update?",
}

consts = {
    "savegames_super_dir": r"Ubisoft Game Launcher\savegames",
    "steam_path": r"Program Files\Steam",
    "config_location": os.path.expandvars(r"%APPDATA%\Wilthon\options.ini"),
    "root": os.path.expandvars(r"%APPDATA%\Wilthon"),
    "steam_grw_path": r"Program Files (x86)\Steam\steamapps\common\Wildlands",
    "ubisoft_grw_path": r"Program Files (x86)\Ubisoft\Ubisoft Game Launcher\games\Tom Clancy's Ghost Recon Wildlands",
    "natives": ("options.ini", "Logs"),
    # {x for x in keyboard._canonical_names.canonical_names.values() if len(x) > 1 and x not in ("enter", "backspace", "shift", "caps lock", "space")}
    "nonchars": {"alt gr", "left ctrl", "right ctrl", "menu", "left windows", "print screen", "tab", "down", "pause",
                 "esc", "left alt", "right windows", "scroll lock", "page down", "windows", "left", "ctrl", "insert",
                 "page up", "right", "alt", "delete", "up", "play/pause media", "num lock"}
}

strings = {
    "restore_options": "(1) Automatically Restore and Relaunch Game\n(2) Exit Wilthon\n(3) Manually Choose Backup to Restore From and Relaunch Game",
    "restore_prompt": "\r{}Select a number 1-3. Choosing ({}) in {}. {}",
    "press_key": "Press any key to exit Wilthon."
}
