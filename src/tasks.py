import ctypes
import multiprocessing
import os
import shutil
import subprocess
import sys
import threading
import time

import keyboard
import winsound

import communication
import strings
import utility
import windows


def get_keyboard_input(current_inp):
    ret = str()
    shift_on = False
    caps_on = False

    while True:
        current_inp.value = ret
        if windows.is_running_from_command_line() and not windows.is_wilthon_focused():
            utility.wait(responsive=True)
            continue

        event = keyboard.read_event(suppress=True)
        is_press = event.event_type == keyboard.KEY_DOWN

        if event.name == "enter":
            return ret

        if "shift" in event.name:
            shift_on = is_press
            continue

        if not is_press:
            continue

        if event.name == "backspace":
            ret = ret[:-1]
        elif len(ret) >= 20:
            continue
        elif event.name == "space":
            ret += " "
        # elif event.name == "tab":
        #     ret += "\t"
        elif event.name == "caps lock":
            caps_on = not caps_on
        elif event.name in strings.consts["nonchars"]:
            continue
        else:
            if shift_on or caps_on:
                ret += event.name.upper()
            else:
                ret += event.name


def get_input(input_given, ret, current_inp):
    sys.stdin = open(0)
    # This is used here instead of the classic `input` to prevent inputs being lost on overwrite.
    inp = get_keyboard_input(current_inp)
    ret.value = inp
    input_given.value = True
    return


def do_countdown(timeout, options, current_inp):
    def printer(i, current_inp=""):
        padding = communication.get_current_padding("input")
        communication.puts("restore_prompt",
                           fmt=(
                               padding, options.default_restore_selection, utility.time_formatter(second=i),
                               current_inp),
                           end="")

    # The input registered through keylogging is not printed onto the console, thus we deal with that manually here.
    if windows.is_running_from_command_line():
        time_left = multiprocessing.Value("i", 0)

        def counter(time_left):
            # 0 won't be printed, it just signifies the timeout.
            time_left.value = options.restore_timeout
            for i in reversed(range(0, options.restore_timeout + 1)):
                time_left.value = i
                time.sleep(1)

        threading.Thread(target=counter, args=(time_left,)).start()
        # Refresh constantly to avoid delay in input: The printout can change with user input whereas normally it
        # only changes once a second, so we decouple them.
        while time_left.value != 0:
            # Clear the input field so that deleted characters don't show up as they are not overwritten otherwise.
            # Not waiting, as clearing the deleted chars needs to be instant.
            utility.wait(responsive=True)
            print(" ", end="")
            printer(time_left.value, current_inp.value)
    else:
        for i in reversed(range(1, options.restore_timeout + 1)):
            printer(i)
            time.sleep(1)

    timeout.value = True
    return


def restore_option_prompt(options):
    input_given = multiprocessing.Value("i", False)
    timeout = multiprocessing.Value("i", False)
    ret = multiprocessing.Manager().Value(ctypes.c_wchar_p, "")
    current_inp = multiprocessing.Manager().Value(ctypes.c_wchar_p, "")

    # As threads cannot be terminated at will, processes are used here.
    inp = multiprocessing.Process(target=get_input, args=(input_given, ret, current_inp))
    inp.start()
    countdown = multiprocessing.Process(target=do_countdown, args=(timeout, options, current_inp))
    countdown.start()

    while True:
        if input_given.value:
            countdown.terminate()
            if windows.is_running_from_command_line():
                print()
            return ret.value
        elif timeout.value:
            inp.terminate()
            print()  # make up for the `end` parameter being `""` and move onto next line
            return str(options.default_restore_selection)
        utility.wait()


def get_restore_option(options, state):
    meaning = {
        "1": (autorestore, options, True),
        "2": (exit_routine, state, False),
        "3": (manual_restore, options, True)
    }
    communication.info("no_grw_running")
    communication.multiline_preprompt("restore_options")
    # `answer_gotten` is set if the user actually focuses the command window and answers, in which case the flashing
    # is stopped by Windows anyway, or an answer is returned from `restore_option_prompt` following a timeout,
    # forcing the cancellation of the flashing to be handled by the code and not Windows.
    answer_gotten = threading.Event()
    if windows.is_running_from_command_line():
        windows.flash_window(answer_gotten)
    while (answer := restore_option_prompt(options)) not in ("1", "2", "3"):
        communication.error("invalid_input")
    answer_gotten.set()
    return meaning[answer]


def restore(options, restoration_index):
    newest_dir_list = list(reversed(sorted(windows.get_backup_list(), key=os.path.getctime)))
    try:
        shutil.copytree(newest_dir_list[restoration_index - 1], options.savegame_location, dirs_exist_ok=True)
    except IndexError:
        # Not enough backups exist.
        communication.info("cannot_restore")
    communication.info("restored")


def autorestore(options):
    restore(options, options.folder_index_to_restore)


def manual_restore(options):
    is_min = options.backup_interval % 60 == 0
    backup_number = len(windows.get_backup_list())
    call = lambda: communication.time_index_question(options.backup_interval, is_min, "manual_restore_question",
                                               "manual_restore_confirmation")
    while (answer := call()) > backup_number:
        communication.error("selected_backup_nonexistent", fmt=(answer, backup_number))
    restore(options, answer)


def exit_routine(state):
    communication.info("quit")
    time.sleep(0.5)  # For visual effect.
    state.exit()


def launch_game(options):
    if options.provider == "steam":
        subprocess.Popen([options.steam_location, "-applaunch", "460930"])
    else:
        os.startfile(options.grw_location)
    communication.info("launching_game")


def do_once(key, action, args):
    def expect_event(key, event_type):
        while True:
            # This is a blocking call, so waiting is unnecessary.
            event = keyboard.read_event()
            if event.event_type == event_type and event.scan_code == keyboard.key_to_scan_codes(key)[0]:
                break

    expect_event(key, keyboard.KEY_DOWN)
    ret = action(*args)
    expect_event(key, keyboard.KEY_UP)
    return ret


def play_sound():
    if utility.is_pyinstaller():
        winsound.PlaySound(os.path.join(sys._MEIPASS, r"resources\beep.wav"), winsound.SND_ALIAS)
    else:
        winsound.PlaySound(r"resources\beep.wav", winsound.SND_ALIAS)


def get_started_player_process():
    p = multiprocessing.Process(target=play_sound)
    p.start()
    return p


def launch_on_launch_check(options, state):
    if windows.is_wildlands_running():
        return
    if options.launch_on_launch:
        launch_game(options)
        state.introduce_immunity()
        return
    communication.info("waiting_for_grw")
    while not windows.is_wildlands_running():
        utility.wait()
    state.introduce_immunity()

# TODO: BB: look into terminatable threads
