import json
import os
import pathlib
import subprocess
import sys
import urllib.request

import requests

import communication
import utility
from version import __version__


# TODO: add code to update options.ini
def check_for_update(options, current_ver_str):
    try:
        latest = json.loads(requests.get("https://codeberg.org/api/v1/repos/surrealcereal/wilthon/releases/latest").text)
    except requests.exceptions.ConnectionError:
        return
    latest_ver = tuple(map(int, latest["tag_name"].split(".")))
    if latest_ver <= tuple(__version__.values()):
        return
    latest_ver_str = latest["tag_name"]
    if options.autoupdate or communication.y_n_prompt("update_question", fmt=latest_ver_str):
        communication.info("updating", fmt=(current_ver_str, latest_ver_str))
        do_update(latest, current_ver_str)


def do_update(latest, current_ver_str):
    remote_exe_info = latest["assets"][0]
    exe_name = remote_exe_info["name"]
    exe = pathlib.Path(os.getcwd()) / exe_name
    # TODO: BB: consider changing the exe name to just Wilthon when downloaded
    urllib.request.urlretrieve(remote_exe_info["browser_download_url"], exe)
    subprocess.Popen([exe, f"lastfile={sys.executable}", f"lastver={current_ver_str}"],
                     creationflags=subprocess.CREATE_NEW_CONSOLE)
    sys.exit()


def cleanup(argv, current_ver_str):
    args = argv[1:]
    if not args or "lastfile=" not in args[0]:
        return
    file = args[0].lstrip("lastfile=")
    last_ver = args[1].lstrip("lastver=")
    os.remove(file)
    communication.info("successful_update", fmt=(last_ver, current_ver_str))


def perform(options):
    if not options.do_updates or not utility.is_pyinstaller():
        return
    current_ver_str = ".".join(map(str, __version__.values()))
    check_for_update(options, current_ver_str)
    cleanup(sys.argv, current_ver_str)
