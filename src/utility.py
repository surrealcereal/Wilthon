import pathlib
import shutil
import sys
import time
import traceback
from functools import wraps

import colorama

import communication
import strings
import windows


def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False


def time_formatter(day=0, hour=0, minute=0, second=0):
    kwargs = locals().copy()
    ret = str()
    for k, v in kwargs.items():
        if v == 0:
            continue
        ret += f"{str(v)} {k}{'s' if v != 1 else ''}, "
    ret = ret[:-2]
    return ret if ret else "less than a second"


def is_installed():
    root = pathlib.Path(strings.consts["root"])
    if not root.exists():
        return False
    for native in strings.consts["natives"]:
        if not (root / native).exists():
            communication.info("unsuccessful_install")
            if communication.y_n_prompt("restarting_install"):
                shutil.rmtree(root)
                return False
            else:
                sys.exit()
    return True


def get_traceback(ex):
    tb = traceback.format_exception(ex.__class__, ex, ex.__traceback__)
    # noinspection PyTypeChecker
    return sum(map(str.splitlines, tb), list())


def is_pyinstaller():
    return getattr(sys, "frozen", False) and hasattr(sys, "_MEIPASS")


def command_line_setup():
    if windows.is_running_from_command_line():
        colorama.just_fix_windows_console()
        windows.set_command_line_title("Wilthon")


def wait(responsive=False):
    time.sleep(0.01 if responsive else 0.1)


def get_var_name(var):
    return f"{var=}".split("=")[0]


def guarded(uses_state=True):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            state = args[-1] if uses_state else None
            try:
                func(*args, **kwargs)
            except Exception as ex:
                communication.exception(ex)
                if state is not None and state.is_terminatable(func.__name__):
                    state.am_ready(func.__name__)
                communication.handle_exception()
                if state is not None:
                    state.exit()

        return wrapper

    return decorator
