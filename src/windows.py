import ctypes
import ctypes.wintypes
import datetime
import os
import pathlib
import threading
import time

import psutil

import strings
import utility


# This function stays ctypes free, as it only called a few times, so the performance penalty incurred is negligible.
def find_exe_path(exe_name):
    for proc in psutil.process_iter():
        if proc.name() == exe_name:
            return pathlib.Path(proc.exe())


MAX_PATH = 260
TH32CS_SNAPPROCESS = 0x00000002


class PROCESSENTRY32(ctypes.Structure):
    _fields_ = (("dwSize", ctypes.wintypes.DWORD),
                ("cntUsage", ctypes.wintypes.DWORD),
                ("th32ProcessID", ctypes.wintypes.DWORD),
                ("th32DefaultHeapID", ctypes.POINTER(ctypes.wintypes.ULONG)),
                ("th32ModuleID", ctypes.wintypes.DWORD),
                ("cntThreads", ctypes.wintypes.DWORD),
                ("th32ParentProcessID", ctypes.wintypes.DWORD),
                ("pcPriClassBase", ctypes.wintypes.LONG),
                ("dwFlags", ctypes.wintypes.DWORD),
                ("szExeFile", ctypes.c_char * MAX_PATH)
                )

    def __init__(self, *args, **kwargs):
        self.dwSize = ctypes.sizeof(self)
        super(PROCESSENTRY32, self).__init__(*args, **kwargs)


# An implementation as follows is also possible, but consumes far too much CPU to be viable to run constantly:
# def process_running(exe_name):
#     for proc in psutil.process_iter():
#         if proc.name() == exe_name:
#             return True
#     return False

def process_running(exe_name):
    def do_check(process_info):
        if bytes(exe_name, "utf-8") == process_info.szExeFile:
            ctypes.windll.kernel32.CloseHandle(processes_snapshot)
            return True

    process_info = PROCESSENTRY32()
    processes_snapshot = ctypes.windll.kernel32.CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,
                                                                         0)
    ctypes.windll.kernel32.Process32First(processes_snapshot, ctypes.byref(process_info))
    if do_check(process_info):
        return True
    while ctypes.windll.kernel32.Process32Next(processes_snapshot, ctypes.byref(process_info)):
        if do_check(process_info):
            return True
    ctypes.windll.kernel32.CloseHandle(processes_snapshot)
    return False


def is_wildlands_running():
    return process_running("GRW.exe")


def get_title_of_window_handle(window_handle):
    length = ctypes.windll.user32.GetWindowTextLengthW(window_handle)
    buffer = ctypes.create_unicode_buffer(length + 1)
    chars_copied = ctypes.windll.user32.GetWindowTextW(window_handle, buffer, length + 1)
    if chars_copied == 0:
        raise ctypes.WinError()
    return buffer.value if buffer.value else None


def get_focused_window_title():
    return get_title_of_window_handle(ctypes.windll.user32.GetForegroundWindow())


def get_exe_name_of_window_handle(window_handle):
    process_handle = ctypes.windll.oleacc.GetProcessHandleFromHwnd(window_handle)
    buffer = ctypes.create_unicode_buffer(MAX_PATH)
    chars_copied = ctypes.windll.psapi.GetProcessImageFileNameW(process_handle, buffer, MAX_PATH)
    # Calling GetProcessHandleFromHwnd on Task Manager seems to fail. TODO: BB: investigate why
    if chars_copied == 0 and get_focused_window_title() != "Task Manager":
        raise ctypes.WinError()
    exe_path = pathlib.Path(buffer.value)
    return exe_path.name if exe_path.name else None


def get_focused_exe():
    return get_exe_name_of_window_handle(ctypes.windll.user32.GetForegroundWindow())


def is_wildlands_focused():
    return get_focused_exe() == "GRW.exe"


def is_cmd_focused():
    cmd_window_handle = ctypes.windll.kernel32.GetConsoleWindow()
    current_window_handle = ctypes.windll.user32.GetForegroundWindow()
    return cmd_window_handle == current_window_handle


def is_running_from_command_line():
    # Also returns `True` for Windows Terminal and/or Powershell, as well as the classic cmd.
    return bool(ctypes.windll.kernel32.GetConsoleWindow())


def is_running_from_wt():
    return get_exe_name_of_window_handle(ctypes.windll.kernel32.GetConsoleWindow()) == "OpenConsole.exe"


def is_wt_focused():
    return get_focused_exe() == "WindowsTerminal.exe"


def is_wilthon_focused():
    if is_running_from_wt():
        return is_wt_focused()
    else:
        return is_cmd_focused()


def get_wt_window_handle():
    # Even though Windows Terminal does not work with `GetConsoleWindow`, we can find our window handle by looking
    # through every window handle and checking if their title is Wilthon. see: https://stackoverflow.com/a/70085212
    def foreach_window(window_handle, _):
        # As the return values are for enumeration control, the actual result is passed with a `nonlocal`.
        nonlocal result
        # Check if the handle points to a valid process.
        if not ctypes.windll.oleacc.GetProcessHandleFromHwnd(window_handle):
            return True  # continue enumerating
        if get_exe_name_of_window_handle(window_handle) == "WindowsTerminal.exe" and get_title_of_window_handle(
                window_handle) == "Wilthon":
            result = window_handle
            return False  # end enumeration

    # Returns bool, accepts two int pointers for hwnd and lParam respectively.
    EnumWindowsProc = ctypes.WINFUNCTYPE(ctypes.c_bool, ctypes.wintypes.LPARAM, ctypes.wintypes.LPARAM)
    result = 0
    ctypes.windll.user32.EnumWindows(EnumWindowsProc(foreach_window), 0)
    if not result:
        ctypes.WinError(ctypes.get_last_error())
    return result


def set_command_line_title(title):
    ctypes.windll.kernel32.SetConsoleTitleW(title)


FLASHW_STOP = 0
FLASHW_TRAY = 2


class FLASHWINFO(ctypes.Structure):
    _fields_ = (('cbSize', ctypes.wintypes.UINT),
                ('hwnd', ctypes.wintypes.HWND),
                ('dwFlags', ctypes.wintypes.DWORD),
                ('uCount', ctypes.wintypes.UINT),
                ('dwTimeout', ctypes.wintypes.DWORD))

    def __init__(self, hwnd, flags=FLASHW_TRAY, count=1e6, timeout_ms=0):
        self.cbSize = ctypes.sizeof(self)
        self.hwnd = hwnd
        self.dwFlags = flags
        self.uCount = count
        self.dwTimeout = timeout_ms
        super(FLASHWINFO, self).__init__()


class Flasher:
    def __init__(self, window_handle=None, count=1_000_000):
        if window_handle:
            self.window_handle = window_handle
        else:
            self.window_handle = ctypes.windll.kernel32.GetConsoleWindow()
        self.winfo = FLASHWINFO(self.window_handle, count=count)

    def _api_call(self):
        return ctypes.windll.user32.FlashWindowEx(ctypes.byref(self.winfo))

    flash = _api_call

    def unflash(self):
        self.winfo.uCount = 0
        self.winfo.dwFlags = FLASHW_STOP
        last_state = self._api_call()
        if last_state == 0: self._api_call()


def flash_window(answer_gotten):
    def do(answer_gotten):
        if is_running_from_wt():
            flasher = Flasher(get_wt_window_handle())
        else:
            flasher = Flasher()

        def conditions():
            return is_wilthon_focused() or answer_gotten.is_set()

        if not conditions():
            flasher.flash()
        while not conditions():
            utility.wait()
            continue
        flasher.unflash()

    threading.Thread(target=do, args=(answer_gotten,)).start()


def dir_contains_file(path, filename):
    return (path / filename).exists()


def get_subdirs(upper_dir):
    return [x for x in upper_dir.iterdir() if x.is_dir()]


def get_backup_list():
    root = pathlib.Path(strings.consts["root"])
    return [dir for dir in get_subdirs(root) if dir.name not in strings.consts["natives"]]


def get_drives():
    drives = list()
    for letter in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
        path = pathlib.Path(letter + ":\\")
        if path.exists():
            drives.append(path)
    return drives


def get_dir_age_str(path):
    now = time.time()
    path_ct = os.path.getctime(path)
    path_age = now - path_ct
    timedelta = datetime.timedelta(seconds=path_age)
    days = timedelta.days
    hours, remainder = divmod(timedelta.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    return utility.time_formatter(day=days, hour=hours, minute=minutes, second=seconds)


def scan_code_to_key(scan_code):
    buffer = ctypes.create_unicode_buffer(32)
    ctypes.windll.user32.GetKeyNameTextW(scan_code << 16, buffer, 32)
    return buffer.value
